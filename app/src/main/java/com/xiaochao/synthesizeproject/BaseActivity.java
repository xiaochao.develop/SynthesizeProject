package com.xiaochao.synthesizeproject;

import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

public abstract class BaseActivity extends AppCompatActivity {

    private LinearLayout parentLinearLayout;
    private Toolbar toolbar;
    public String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName();
        initContentView(R.layout.toolbar_include);
        setContentView(getLayoutId());
        toolbar = findViewById(R.id.toolbar);
        ///////////////////////////////////////下面两行顺序不能变，变了默认会有Title
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ///////////////////////////////////
        if(isToolBarBack()) initToolBarBack();
        initView();
        init();
    }

    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void init();

    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void openActivity(Class clz){
        Intent intent = new Intent(this,clz);
        startActivity(intent);
    }

    public void openActivity(Class clz,Bundle bundle){
        Intent intent = new Intent(this,clz);
        if(bundle!=null){
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * 将toolbar添加到父布局
     * @param layoutResID
     */
    private void initContentView(@LayoutRes int layoutResID) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        viewGroup.removeAllViews();
        parentLinearLayout = new LinearLayout(this);
        parentLinearLayout.setOrientation(LinearLayout.VERTICAL);
        //  add parentLinearLayout in viewGroup
        viewGroup.addView(parentLinearLayout);
        //  add the layout of BaseActivity in parentLinearLayout
        LayoutInflater.from(this).inflate(layoutResID, parentLinearLayout, true);
    }

    /**
     * 将设置的布局添加到父布局中
     * @param layoutResID
     */
    @Override
    public void setContentView(int layoutResID) {
        LayoutInflater.from(this).inflate(layoutResID, parentLinearLayout, true);
    }

    /**
     * 返回toolbar
     * @return
     */
    protected Toolbar getToolBar() {
        return toolbar;
    }

    /**
     * 设置toolbar标题
     * @param toolBarTitle
     */
    protected void setToolBarTitle(CharSequence toolBarTitle) {
        if (null != toolBarTitle) {
            toolbar.setSubtitle(toolBarTitle);
        } else {
            toolbar.setSubtitle("");
        }
    }

    /**
     * 是否要设置toolbar返回箭头，默认为true
     *
     */
    protected boolean isToolBarBack(){
        return true;
    }

    /**
     * 处理toolbar返回箭头返回事件
     */
    protected void initToolBarBack(){
//        setSupportActionBar(toolbar);
        //设置toolbar回退
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    /**
     * 设置toolbar返回事件
     * @param toolBar
     * @param webView
     */
    protected void setToolBarBack(Toolbar toolBar,  final WebView webView){
        if(toolBar!=null){
            setSupportActionBar(toolBar);
            //设置toolbar回退
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (webView != null) {
                        if (webView.canGoBack()) {
                            webView.goBack();
                        } else {
                            finish();
                        }
                    } else {
                        finish();
                    }
                }
            });
        }
    }
}
