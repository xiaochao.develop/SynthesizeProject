package com.xiaochao.synthesizeproject;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(getLayoutId(), container, false);
        initView(view);
        init();
        return view;
    }

    public abstract int getLayoutId();

    public abstract void initView(View view);

    public abstract void init();

    public void showToast(String msg){
        if(!TextUtils.isEmpty(msg)){
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        }
    }

    public void openActivity(Class toClz){
        Intent intent = new Intent(getActivity(),toClz);
        startActivity(intent);
    }

}
