package com.xiaochao.synthesizeproject.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.xiaochao.synthesizeproject.R;

import java.util.List;

/**
 * Created by xiaochao on 2018/6/10.
 */

public class MainRecycleviewAdapter extends BaseQuickAdapter<String,BaseViewHolder> {

    public MainRecycleviewAdapter(int layoutResId, @Nullable List<String> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_content,item);
    }
}
