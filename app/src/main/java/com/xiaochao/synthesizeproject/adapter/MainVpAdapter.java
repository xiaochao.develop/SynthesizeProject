package com.xiaochao.synthesizeproject.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xiaochao.synthesizeproject.BaseFragment;

import java.util.List;

public class MainVpAdapter extends FragmentPagerAdapter {

    private List<BaseFragment>  baseFragmentList;

    public MainVpAdapter(FragmentManager fm,List<BaseFragment>  baseFragmentList) {
        super(fm);
        this.baseFragmentList = baseFragmentList;
    }

    @Override
    public BaseFragment getItem(int position) {
        return baseFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return baseFragmentList.size();
    }
}
