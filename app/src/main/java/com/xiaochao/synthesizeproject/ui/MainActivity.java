package com.xiaochao.synthesizeproject.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.xiaochao.synthesizeproject.BaseActivity;
import com.xiaochao.synthesizeproject.BaseFragment;
import com.xiaochao.synthesizeproject.R;
import com.xiaochao.synthesizeproject.adapter.MainVpAdapter;
import com.xiaochao.synthesizeproject.ui.fragment.AndroidFragment;
import com.xiaochao.synthesizeproject.ui.fragment.CustomViewFragment;
import com.xiaochao.synthesizeproject.ui.fragment.DesignModeFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private BaseFragment androidFragment, designModeFragment, custsomeViewFragment;
    private BottomNavigationView navigation;
    private String[] subTitles ;
    private MenuItem menuItem;

    @Override
    protected int getLayoutId() {
        return R.layout.main_activity;
    }

    @Override
    protected boolean isToolBarBack() {
        return false;
    }

    @Override
    protected void initView() {
        viewPager = findViewById(R.id.vp_main);
        toolbar = getToolBar();
        setToolBarTitle(getString(R.string.title_android));
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected void init() {
        subTitles = new String[]{getString(R.string.title_android),getString(R.string.title_design_mode),getString(R.string.title_custom_view)};
        List<BaseFragment> baseFragmentList = new ArrayList<>();
        if (androidFragment == null) androidFragment = new AndroidFragment();
        if (designModeFragment == null) designModeFragment = new DesignModeFragment();
        if (custsomeViewFragment == null) custsomeViewFragment = new CustomViewFragment();
        baseFragmentList.add(androidFragment);
        baseFragmentList.add(designModeFragment);
        baseFragmentList.add(custsomeViewFragment);
        viewPager.setAdapter(new MainVpAdapter(getSupportFragmentManager(), baseFragmentList));
        initListener();
    }

    private void initListener(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageScrollStateChanged(int state) {}

            @Override
            public void onPageSelected(int position) {
                if (menuItem != null) {
                    menuItem.setChecked(false);
                } else {
                    navigation.getMenu().getItem(0).setChecked(false);
                }
                menuItem = navigation.getMenu().getItem(position);
                menuItem.setChecked(true);
                setToolBarTitle(subTitles[position]);
            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_android:
                    setToolBarTitle(subTitles[0]);
                    viewPager.setCurrentItem(0,false);
                    return true;
                case R.id.navigation_design_mode:
                    setToolBarTitle(subTitles[1]);
                    viewPager.setCurrentItem(1,false);
                    return true;
                case R.id.navigation_custome_view:
                    setToolBarTitle(subTitles[2]);
                    viewPager.setCurrentItem(2,false);
                    return true;
            }
            return false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                openActivity(SearchActivity.class);
                overridePendingTransition(0,0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
