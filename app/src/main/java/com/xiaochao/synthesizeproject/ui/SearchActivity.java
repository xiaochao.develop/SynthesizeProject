package com.xiaochao.synthesizeproject.ui;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.xiaochao.synthesizeproject.BaseActivity;
import com.xiaochao.synthesizeproject.R;

public class SearchActivity extends BaseActivity {

    private Toolbar toolbar;

    @Override
    protected int getLayoutId() {
        return R.layout.search_activity;
    }

    @Override
    protected void initView() {
        toolbar = getToolBar();
    }

    @Override
    protected void init() {
        setToolBarTitle("搜索");
    }

    @Override
    protected void onPause() {
        overridePendingTransition(0,0);
        super.onPause();
    }
}
