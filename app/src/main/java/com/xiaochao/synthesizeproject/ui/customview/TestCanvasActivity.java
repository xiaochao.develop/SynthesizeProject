package com.xiaochao.synthesizeproject.ui.customview;

import com.xiaochao.synthesizeproject.BaseActivity;
import com.xiaochao.synthesizeproject.R;
import com.xiaochao.synthesizeproject.ui.customview.view.PieView;
import com.xiaochao.synthesizeproject.ui.customview.view.TestCanvasView;

import java.util.ArrayList;

public class TestCanvasActivity extends BaseActivity {

    private PieView mPieView;
    private TestCanvasView mTestCanvasView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test_custom_view;
    }

    @Override
    protected void initView() {
        setToolBarTitle("canvas操作");
        mPieView = findViewById(R.id.pieView);
        ArrayList<PieData> arrayList = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            PieData pieData = new PieData("name" + i, 1);
            arrayList.add(pieData);
        }
        mPieView.setData(arrayList);
        mPieView.setStartAngle(30);
        mTestCanvasView = findViewById(R.id.canvas);
    }

    @Override
    protected void init() {

    }
}
