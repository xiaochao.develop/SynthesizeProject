package com.xiaochao.synthesizeproject.ui.customview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.xiaochao.synthesizeproject.BaseActivity;
import com.xiaochao.synthesizeproject.R;

public class TestPathActivity extends BaseActivity {


    @Override
    protected int getLayoutId() {
        return R.layout.activity_test_path;
    }

    @Override
    protected void initView() {
        setToolBarTitle("Path操作");
    }

    @Override
    protected void init() {

    }
}
