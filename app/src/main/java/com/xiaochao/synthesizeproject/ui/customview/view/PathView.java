package com.xiaochao.synthesizeproject.ui.customview.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * 蜘蛛网图
 */
public class PathView extends View {
    private int count = 6;                //数据个数
    private float angle = (float) (Math.PI * 2 / count);
    private float radius;                   //网格最大半径
    private int centerX;                  //中心X
    private int centerY;                  //中心Y
    private String[] titles = {"a", "b", "c", "d", "e", "f"};
    private double[] data = {100, 60, 60, 60, 100, 50, 10, 20}; //各维度分值
    private float maxValue = 100;             //数据最大值
    private Paint mainPaint;                //雷达区画笔
    private Paint valuePaint;               //数据区画笔
    private Paint textPaint;                //文本画笔

    public PathView(Context context) {
        this(context, null);
    }

    public PathView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PathView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mainPaint = new Paint();
        mainPaint.setStyle(Paint.Style.STROKE);
        mainPaint.setColor(Color.BLACK);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        centerX = w;
        centerY = h;//确定在xml布局中使用的该View的宽高
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate(centerX / 2, centerY / 2);//将画布中心点移动到View的中心位置
//        Path path = new Path();
//        path.lineTo(200, 200);
//
//        path.moveTo(0, 0);//moveTo只改变下次操作的起点
////        path.setLastPoint(0, 0);//setLastPoint是重置上一次操作的最后一个点，这里即表示把第一次path.lineTo(200, 200);重置到了原点
//
//        path.lineTo(200, -200);

//        path.lineTo(200, 200);
////        path.moveTo(0, 20);
//        path.lineTo(200, 0);
//        path.close();//close方法用于连接当前最后一个点和最初的一个点(如果两个点不重合的话)，最终形成一个封闭的图形。
        //如果连接了最后一个点和第一个点仍然无法形成封闭图形，则close什么 也不做。


//        path.addRect(-200,-200,200,200,Path.Direction.CW);//	最后一个参数为顺时针

//        canvas.scale(1,-1);                         // <-- 注意 翻转y坐标轴
//
//        path.lineTo(100,100);
//
//        RectF oval = new RectF(0,0,300,300);
//
//        path.arcTo(oval,0,270);
//        // path.arcTo(oval,0,270,false);             // <-- 和上面一句作用等价
//
//        canvas.drawPath(path,paint);
        drawPolygon(canvas);
        postInvalidate();

    }

    /**
     * 绘制正多边形
     */
    private void drawPolygon(Canvas canvas) {
        Path path = new Path();
        float r = radius / (count - 1);//r是蜘蛛丝之间的间距
        for (int i = 1; i < count; i++) {//中心点不用绘制
            float curR = r * i;//当前半径
            path.reset();
            for (int j = 0; j < count; j++) {
                if (j == 0) {
                    path.moveTo(centerX + curR, centerY);
                } else {
                    //根据半径，计算出蜘蛛丝上每个点的坐标
                    float x = (float) (centerX + curR * Math.cos(angle * j));
                    float y = (float) (centerY + curR * Math.sin(angle * j));
                    path.lineTo(x, y);
                }
            }
            path.close();//闭合路径
            canvas.drawPath(path, mainPaint);
        }
    }
}
