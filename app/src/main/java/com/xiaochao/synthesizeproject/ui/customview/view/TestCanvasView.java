package com.xiaochao.synthesizeproject.ui.customview.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by xiaochao on 2018/6/30.
 */

public class TestCanvasView extends View {

    private Paint mPaint = new Paint();
    private int mWidth, mHeight;

    public TestCanvasView(Context context) {
        this(context, null);
    }

    public TestCanvasView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TestCanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.translate(mWidth / 2, mHeight / 2);
//        RectF rectF = new RectF(-240, -240, 240, 240);
//        for (int i = 0; i < 10; i++) {
//
//            canvas.drawRect(rectF, mPaint);
//            canvas.scale(0.9f, 0.9f);
//        }
        canvas.drawCircle(0, 0, 400, mPaint);
        canvas.drawCircle(0, 0, 360, mPaint);
        for (int i = 0; i < 360; i += 10) {
//            canvas.save();
            canvas.drawLine(0, 360, 0, 400, mPaint);
            canvas.rotate(10);
//            canvas.restore();
        }
    }
}
