package com.xiaochao.synthesizeproject.ui.designmode;

import android.content.Context;

import java.lang.ref.WeakReference;

/**
 * Created by xiaochao on 2018/6/10.
 */

public class SingleClass {
    //饿汉式单例模式
//    private static final SingleClass single = new SingleClass();
//    private SingleClass(){}
//    public static SingleClass getInstance(){
//        return single;
//    }

//    懒汉式单例模式
//    private static SingleClass single = null;
//    private SingleClass(){}
//    public static synchronized SingleClass getInsgtance(){
//        if(single==null){
//            single = new SingleClass();
//        }
//        return single;
//    }

    //双重检索式
    private Context context;
    private static volatile SingleClass single = null;
    private static WeakReference<SingleClass> weakReferenceInstance;
    private SingleClass(Context context){
        this.context = context;
    }
    public static SingleClass getInstance(Context context){
        if(single==null){
            synchronized (SingleClass.class){
                if(single==null){
                    if (weakReferenceInstance == null || weakReferenceInstance.get() == null) {
                        weakReferenceInstance = new WeakReference<SingleClass>(new SingleClass(context));
                    }
                    return weakReferenceInstance.get();
                }
            }
        }
        return single;
    }

    //静态内部类单例模式
//    private SingleClass(){}
//    private static SingleClass getInstance(){
//        return SingletonHolder.single;
//    }
//
//    private static class SingletonHolder{
//        private static final SingleClass single = new SingleClass();
//    }


}
