package com.xiaochao.synthesizeproject.ui.designmode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.xiaochao.synthesizeproject.BaseActivity;
import com.xiaochao.synthesizeproject.R;

/**
 * 单例模式
 */
public class SingletonActivity extends BaseActivity {


    @Override
    protected int getLayoutId() {
        return R.layout.singleton_activity;
    }

    @Override
    protected void initView() {
        setToolBarTitle("单例模式");
    }

    @Override
    protected void init() {

    }
}
