package com.xiaochao.synthesizeproject.ui.designmode.chainofresopnsibility;

import com.xiaochao.synthesizeproject.BaseActivity;
import com.xiaochao.synthesizeproject.R;

public class XiaoQun extends BaseActivity {


    @Override
    protected int getLayoutId() {
        return R.layout.chain_of_responsibility_model_activity;
    }

    @Override
    protected void initView() {
        setToolBarTitle("责任链模式");
    }

    @Override
    protected void init() {
        GroupLeader groupLeader = new GroupLeader();
        Director director = new Director();
        Manager manager = new Manager();
        Boss boss = new Boss();

        //设置上一级领导（处理者）对象
        groupLeader.nextHandler = director;
        director.nextHandler = manager;
        manager.nextHandler = boss;

        //发起报账申请
        groupLeader.handleRequest(100000);
    }
}
