package com.xiaochao.synthesizeproject.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xiaochao.synthesizeproject.BaseFragment;
import com.xiaochao.synthesizeproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AndroidFragment extends BaseFragment {

    private RecyclerView mRecyclerView;

    @Override
    public int getLayoutId() {
        return R.layout.android_fragment;
    }

    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rl_android);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void init() {

    }

}
