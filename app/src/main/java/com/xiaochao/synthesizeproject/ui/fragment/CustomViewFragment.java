package com.xiaochao.synthesizeproject.ui.fragment;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.xiaochao.synthesizeproject.BaseFragment;
import com.xiaochao.synthesizeproject.R;
import com.xiaochao.synthesizeproject.adapter.MainRecycleviewAdapter;
import com.xiaochao.synthesizeproject.ui.customview.TestCanvasActivity;
import com.xiaochao.synthesizeproject.ui.customview.TestPathActivity;
import com.xiaochao.synthesizeproject.ui.customview.TouchDispatchActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomViewFragment extends BaseFragment {

    private RecyclerView mRecyclerView;

    @Override
    public int getLayoutId() {
        return R.layout.custom_view_fragment;
    }

    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rl_custom_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void init() {
        String[] contents = new String[]{"Touch事件分发机制", "Canvas操作", "Path操作"};
        List<String> list = new ArrayList<>();
        list.addAll(Arrays.asList(contents));
        MainRecycleviewAdapter adapter = new MainRecycleviewAdapter(R.layout.string_recycle_item, list);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(onItemClickListener);
    }

    BaseQuickAdapter.OnItemClickListener onItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            switch (position) {
                case 0:
                    openActivity(TouchDispatchActivity.class);
                    break;
                case 1:
                    openActivity(TestCanvasActivity.class);
                    break;
                case 2:
                    openActivity(TestPathActivity.class);
                    break;
            }
        }
    };

}
