package com.xiaochao.synthesizeproject.ui.fragment;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.xiaochao.synthesizeproject.BaseFragment;
import com.xiaochao.synthesizeproject.R;
import com.xiaochao.synthesizeproject.adapter.MainRecycleviewAdapter;
import com.xiaochao.synthesizeproject.ui.designmode.chainofresopnsibility.XiaoQun;
import com.xiaochao.synthesizeproject.ui.designmode.SingletonActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DesignModeFragment extends BaseFragment {

    private RecyclerView mRecyclerView;

    @Override
    public int getLayoutId() {
        return R.layout.design_mode_fragment;
    }

    @Override
    public void initView(View view) {
        mRecyclerView = view.findViewById(R.id.rl_design_mode);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void init() {
        String[] contents = new String[]{"单例模式", "Buidler模式", "原型模式", "工厂方法模式",
                "抽象工厂模式", "策略模式", "状态模式", "责任链模式", "解释器模式", "命令模式",
                "观察者模式", "备忘录模式", "迭代器模式", "模板方法模式", "访问者模式", "中介者模式",
                "代理模式", "组合模式", "适配器模式", "装饰模式", "享元模式", "外观模式", "桥接模式"};
        List<String> contentList = new ArrayList<>();
        contentList.addAll(Arrays.asList(contents));
        MainRecycleviewAdapter adapter = new MainRecycleviewAdapter(R.layout.string_recycle_item, contentList);
        mRecyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(mOnItemClickListener);
    }

    BaseQuickAdapter.OnItemClickListener mOnItemClickListener = new BaseQuickAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
            switch (position) {
                case 0:
                    openActivity(SingletonActivity.class);
                    break;
                case 7:
                    openActivity(XiaoQun.class);
                    break;
            }
        }
    };
}
